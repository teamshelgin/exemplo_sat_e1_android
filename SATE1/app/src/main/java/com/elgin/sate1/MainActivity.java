package com.elgin.sate1;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

import br.com.elgin.Sat;


/** @author Gasperin **/
public class MainActivity extends AppCompatActivity {

    private final String CODIGO_ATIVACAO = "00000000"; // Troque pelo codigo de ativacao do SAT...

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this.getApplicationContext();

        Button btnConsultarSAT = findViewById(R.id.btnConsultarSAT);
        btnConsultarSAT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultarSAT();
            }
        });

        Button btnConsultarStatus = findViewById(R.id.btnConsultarStatus);
        btnConsultarStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultarStatus();
            }
        });

        Button btnEnviarVenda = findViewById(R.id.btnEnviarVenda);
        btnEnviarVenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviarVenda();
            }
        });
    }

    private void consultarSAT() {
        String retorno = Sat.consultarSat(getNumeroSessao());
        mostrarRetorno(retorno);
    }

    private void consultarStatus() {
        String retorno = Sat.consultarStatusOperacional(getNumeroSessao(), CODIGO_ATIVACAO);
        mostrarRetorno(retorno);
    }

    private void enviarVenda() {
        final String dadosVenda = "<?xml version=\"1.0\"?> <CFe> <infCFe versaoDadosEnt=\"0.07\"> <ide><CNPJ>16716114000172</CNPJ><signAC>SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT</signAC><numeroCaixa>001</numeroCaixa> </ide><emit><CNPJ>14200166000166</CNPJ><IE>111111111111</IE><indRatISSQN>N</indRatISSQN></emit><dest></dest><det nItem=\"1\"> <prod> <cProd>00000000000001</cProd> <xProd>PRODUTO NFCE 1</xProd> <NCM>94034000</NCM> <CFOP>5102</CFOP> <uCom>UN</uCom> <qCom>1.0000</qCom> <vUnCom>3.51</vUnCom> <indRegra>T</indRegra> </prod> <imposto> <ICMS><ICMS00><Orig>0</Orig><CST>00</CST><pICMS>7.00</pICMS></ICMS00> </ICMS><PIS><PISAliq><CST>01</CST><vBC>6.51</vBC><pPIS>0.0165</pPIS></PISAliq> </PIS> <COFINS><COFINSAliq><CST>01</CST><vBC>6.51</vBC><pCOFINS>0.0760</pCOFINS></COFINSAliq> </COFINS> </imposto> </det> <total><DescAcrEntr><vDescSubtot>0.51</vDescSubtot></DescAcrEntr><vCFeLei12741>0.56</vCFeLei12741></total><pgto> <MP> <cMP>01</cMP> <vMP>6.51</vMP> </MP></pgto><infAdic> <infCpl>Trib aprox R$ 0,36 federal, R$ 1,24 estadual e R$ 0,00 municipal&lt;br&gt;CAIXA: 001 OPERADOR: ROOT</infCpl></infAdic></infCFe></CFe>";
        String retorno = Sat.enviarDadosVenda(getNumeroSessao(), CODIGO_ATIVACAO, dadosVenda);
        mostrarRetorno(retorno);
    }

    private int getNumeroSessao() {
        return new Random().nextInt(1_000_000);
    }

    private void mostrarRetorno(String retorno) {
        Toast.makeText(mContext, String.format("Retorno: %s", retorno), Toast.LENGTH_LONG).show();
    }
}
